#Temple

Template compiler for pre-processing CSS, JS, HTML or any other text files.

Source files are treated as Go templates with custom variable delimiters "[[" and "]]" to avoid conflicts with AngularJS and other template engine syntaxes.

## Installation  
Clone the repo, cd into the project dir and run go install.  
If you've added $GOPATH/bin to your path you should now be able to run: `temple -s sourcedir -o outputdir -d data`  
-v flag enables dev mode with log statements
