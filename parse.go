// Temple
// A template compiler for pre-processing js, css and html files to be served statically
// Copyright Roar Film Pty Ltd 2015-2016
// All rights reserved. No re-use of this source code allowed without prior written agreement from Roar Film

package temple

import (
	"bufio"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"text/template"
)

// ParseTemplates takes source (i) and desination (o) directories and data to be applied to templates
// all files within the source dir will be parsed as Go templates using `data`
func ParseAll(i, o string, data interface{}) error {
	// define custom delimiters to avoid conflicts with {{}} (eg in case templates are for AngularJS)
	const (
		lDelim = "[["
		rDelim = "]]"
	)

	//parse templates
	err := filepath.Walk(i, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if info.IsDir() {
			// unless we're looking at the root src dir create a matching subdir in build
			if !strings.HasSuffix(i, info.Name()) {
				ferr := os.Mkdir(o+string(filepath.Separator)+info.Name(), 0755)
				if ferr != nil && !strings.HasSuffix(ferr.Error(), "file exists") {
					fmt.Println("Failed to create output sub-directory :: ", ferr.Error())
				}
			}
			return nil
		}

		var t *template.Template
		tn := path[len(i)+1:]
		fn := tn
		sd := strings.LastIndex(tn, "/")
		if sd >= 0 {
			tn = tn[sd+1:]
		}
		if t == nil {
			t = template.New(tn)
			t.Delims(lDelim, rDelim)
			_, err = t.ParseFiles(path)
		} else {
			_, err = t.New(tn).ParseFiles(path)
		}

		op := o + "/" + fn
		f, ferr := os.OpenFile(op, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0755)
		if ferr != nil {
			fmt.Printf("Error opening file %v", ferr)
		} else {
			w := bufio.NewWriter(f)
			werr := t.Execute(w, data)
			if werr != nil {
				fmt.Printf("Whoops || %v\n", werr)
			}
			w.Flush()
			f.Close()
		}
		return err
	})

	return err
}
