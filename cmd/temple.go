// Temple
// A template compiler for pre-processing js, css and html files to be served statically
// Copyright Roar Film Pty Ltd 2015
// All rights reserved. No re-use of this source code allowed without prior written agreement from Roar Film

package main

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"time"

	"magnus.roareducate.com/lib/temple"
)

var devMode bool

// string separators for template data string
const (
	pSep  = ";"
	kvSep = "|"
)

const helpString = `Temple - template compiler.

Usage:
temple -s source-dir -o output-dir -d data

data is a list of key value pairs in the following format:
key and value separated by pipe (|), and pairs separated by semi-colon (;)
v1|d1;v2|d2...
`

func main() {
	// flag variables for input
	var srcDir = flag.String("s", "", "Template source directory")
	var outDir = flag.String("o", "", "Template output directory")
	var inData = flag.String("d", "", "Template data")
	flag.Parse()

	var startTime time.Time
	startTime = time.Now()
	fmt.Println("Starting Temple template compiler")

	// require src and out dirs, and template data be provided as flags
	if *srcDir == "" || *outDir == "" || *inData == "" {
		fmt.Println(helpString)
		os.Exit(0)
	}

	// check if source directory exists, print errror and exit if not
	src, err := os.Stat(*srcDir)
	if os.IsNotExist(err) {
		fmt.Printf("Template source directory `%s` does not exist", *srcDir)
		os.Exit(0)
	}

	// and make sure it is a directory, otherwise print error and exit
	if !src.IsDir() {
		fmt.Printf("Template source `%s` is not a directory", *srcDir)
		os.Exit(0)
	}

	// check if the output directory exists, if not try to create it
	out, err := os.Stat(*outDir)
	if os.IsNotExist(err) {
		ferr := os.MkdirAll("."+string(filepath.Separator)+*outDir, 0755)
		if ferr != nil {
			fmt.Printf("Failed to create output directory `%v`", ferr)
			os.Exit(0)
		}
	} else {
		// if target exists but is not a directory print an error and exit
		if !out.IsDir() {
			fmt.Printf("Output target exists but is not a directory")
			os.Exit(0)
		}
	}
	// map to hold template data
	data := make(map[string]string)

	// parse input data: split key/val pairs then add each to data map
	pairs := strings.Split(*inData, pSep)
	for _, pair := range pairs {
		p := strings.Split(pair, kvSep)
		data[p[0]] = p[1]
	}

	// use absolute paths for src and target dirs
	ai, _ := filepath.Abs(*srcDir)
	ao, _ := filepath.Abs(*outDir)

	// and we're off...
	err = temple.ParseAll(ai, ao, data)
	if err != nil {
		fmt.Println("Error while parsing templates : ", err)
		os.Exit(1)
	}

	endTime := time.Now()
	runTime := endTime.Sub(startTime)
	fmt.Println("Temple compile run complete in ", runTime)
}
